#include <iostream>

volatile unsigned int *data = (volatile unsigned int *)0x80000a00;
volatile unsigned int *output = (volatile unsigned int *)0x80000a04;
volatile unsigned int *direction = (volatile unsigned int *)0x80000a08;

int main(void){
	  // Enable all Outputs
	  *direction = 0xffffffff;
	  // Assign value to output registers
	  *output = 0x0000002A;
	  std::cout << "Current value of gpio lines: 0x" << *data << std::endl;
	return 0;
}
