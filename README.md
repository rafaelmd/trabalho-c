Este respositório contém os códigos utilizados para o projeto da disciplina de "Programação em C++ para sistemas embarcados" oferecida pelo Dep. de Engenharia Elétrica da Universidade Federal de Santa Catarina.

Os objetivos do projeto estão detalhadamente descritos na página do prof. Bezerra em:
http://gse.ufsc.br/bezerra/?page_id=1361