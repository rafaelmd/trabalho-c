/*
   File List.h

   Header file for class list

   Class List stores Nodes (class Node) in a linked list.
   
   This file has the List's interface (header file).

   Class list is definition for a linked list, having the following operations:

   1. Initialize the list to an empty list.
   2. Free the nodes of a list.
   3. Determine whether a list is empty.
   4. (NOT YET!) Add a node with a given value into the list following
      the first node with another given value.
   5. Add a node with a given value to the front of the list.
   6. Add a node with a given value to the end of the list.
   7. Delete the first node from the list.

   Eduardo Augusto Bezerra <bezerra@inf.pucrs,br>
   Faculdade de Informatica, PUC-RS
   Laboratorio de Programacao II (EC)

   Abril de 2006.

*/

#include "Node.cpp"

class List : public ClockCalendar 
{

	Node* head;
	Node* tail;	
	Node* atual;

	public:

	/*Construtor*/  
	List();
	/*Construtor*/	
	~List();
	
	/*Insere um no no head da lista*/
	void insertBeforeFirst(int dat);

	/*Le o no que o ponteiro atual aponto*/
	int readAtual();

	/*Direciona o ponteiro atual para direita ou da esquerda do no que ele
	  esta apontando:
	  direct = 0 -->  direciona para ESQUERDA
	  direct = 1 -->  direciona para DIREITA     
	
	  obs: Depois da ultima posicao da lista ele passa para a primeira 
	       e vice-versa.
	*/
	void setAtual(int direct);

	void readTime (int& h, int& s, int& m, bool& pm, int& mn, int& d, int& y);
	void setTime(int h, int s, int m, bool pm, int mn, int d, int y);
};

