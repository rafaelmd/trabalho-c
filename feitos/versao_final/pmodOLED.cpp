#include <iostream>
#include <unistd.h>
#include <time.h>
#include "caracteres.h"

#define CS 24   // SPI Chip Select (Slave Select)
#define SDIN 25 // SPI Data In (MOSI)
#define SCLK 27 // SPI Clock 
#define DC 28   // Data/Command Control 
#define RES 29
#define VBATC 30
#define VDDC 31

inline void msleep(clock_t aux) 
{
	clock_t msec = 1000*aux;
	clock_t start_time = clock();
	clock_t end_time = msec  + start_time;
	while(clock() != end_time);
} 

inline void usleep(clock_t aux) 
{
	clock_t msec = aux;
	clock_t start_time = clock();
	clock_t end_time = msec  + start_time;
	while(clock() != end_time);
} 
void ssd1306_command(int comando)
{	
	*output&=~(1<<DC); 	// Seta o pino DC para LOW (modo comando)
	*output&=~(1<<CS); 	// Seta o pino CS para LOW
	usleep(20);

	for(int i=0; i<=7; i++)
	{				
		*output&=~(1<<SCLK);	//Seta o pino SCLK para LOW
					
		if(comando & (0x80>>i))	*output|=(1<<SDIN);     //envia 1
		else *output&=~(1<<SDIN);	//envia 0
						
		usleep(20);	//delay para garantir integridade do bit
					
		*output|=(1<<SCLK);  		//SCLK=1
		usleep(20);
					
	}	
	*output&=~(1<<SCLK);	//SCLK=0
	*output&=~(1<<SDIN);	//SDIN=0
	*output|=(1<<CS);  	//CS=1
}



void ssd1306_data(char dado)
{
	*output|=(1<<DC); 	// Seta o pino DC para HIGH (modo dado)
	*output&=~(1<<CS); 	// Seta o pino CS para LOW
	usleep(20);

	for(int i=0; i<=7; i++)
	{				
		*output&=~(1<<SCLK);	//SCLK -> LOW
					
		if(dado & (0x80>>i))	*output|=(1<<SDIN);     //envia 1
		else *output&=~(1<<SDIN);	//envia 0
						
		usleep(20);	//delay para garantir integridade do bit
					
		*output|=(1<<SCLK);  	//SCLK -> HIGH

		usleep(20);
					
	}	
	*output&=~(1<<SCLK);	//SCLK -> LOW
	*output&=~(1<<SDIN);	//SDIN -> LOW
	*output|=(1<<CS);  	//CS -> HIGH
}

void ssd1306_poweron(void)
{
/*	Power ON sequence: 
	1. Power ON VDD 
	2. After VDD become stable, set RES# pin LOW (logic low) for at least 3us (t1) (4) and then HIGH (logic 
	high). 
	3. After set RES# pin LOW (logic low), wait for at least 3us (t2). Then Power ON VCC.
	(1)
	4. After VCC become stable, send command AFh for display ON. SEG/COM will be ON after 100ms 
	(tAF). 
*/


	*output&=~(1<<VDDC); // Liga o VDD (Ativo em baixo)

	msleep(1); // Depois de ligar no VDD deve-se esperar um tempo ate estabilizar

	
	// reseta o display
	*output&=~(1<<RES);					// seta sa�da de reset = 0
	msleep(1);  
	*output|=(1<<RES);
	

	ssd1306_command(0xAE);	// Desliga o display
	
	//O charge pump deve ser habilitado da seguinte maneira
		
	ssd1306_command(0x8D);  // Disabilita o charge pump
	ssd1306_command(0x14);	// Disabilita o charge pump
		
	ssd1306_command(0xD9);
	ssd1306_command(0xF1);
	msleep(1);
		
	ssd1306_command(0xAF);
	msleep(100);


	*output&=~(1<<VBATC); // LIga VBAT (Ativo em alto)	
	msleep(100);
	
	ssd1306_command(0xA0);
	ssd1306_command(0xC0);
	
	ssd1306_command(0xDA); //Seta COM
	ssd1306_command(0X20); //Sequencial COM, esquerda
	
	ssd1306_command(0xAF);
 	      
}


void ssd1306_clear()
{
	unsigned char i,j;

	for(i=4;i<8;i++)	
	{
		ssd1306_command (0xb0+ i );
		ssd1306_command (0x00);
		ssd1306_command (0x10);
		
		for(j=0;j<128;j++)	
		{
			ssd1306_data(0x00);
			msleep(1);
		}
	}		
}

void sendchar(char simbol)
{	int *aux;
	aux=&caracteres[0] +8*simbol;
	for(int j=0;j<8;j++)
	{	
		
		ssd1306_data(*(aux+j));
		
	}
}
void sendString(char string[])
{	char k=0;
	do
	{
		
		sendchar(string[k]);
		k++;
		
	}while(string[k]!=0);
	
}

void display(int valor, int h, int m, int s, bool pm, int mn, int d, int y)
{
	char buffer [50];
	
	sprintf (buffer, "Reg: %d        ", valor);

		ssd1306_command (0xb4);	 
		ssd1306_command (0x00);
		ssd1306_command (0x10);
		sendString(buffer);
	
	if(pm==1){
	sprintf (buffer, "Hr-%d: %d: %d  PM ", h, m, s/10);
	}
	else{
	sprintf (buffer, "Hr-%d: %d: %d  AM ", h, m, s/10);
	}
	
		ssd1306_command (0xb5);	
		ssd1306_command (0x00);
		ssd1306_command (0x10);
		sendString(buffer);

		
	sprintf (buffer, "D-%d/ %d/ %d", mn, d, y);
			ssd1306_command (0xb6);
		ssd1306_command (0x00);
		ssd1306_command (0x10);
		sendString(buffer);
		
	sprintf (buffer, "----------------");
		
		ssd1306_command (0xb7);
		ssd1306_command (0x00);
		ssd1306_command (0x10);
		sendString(buffer);

}
