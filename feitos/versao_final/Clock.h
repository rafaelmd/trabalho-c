class Clock 
{ 

	protected:
 
//Atributos
		
		int _hr, _min, _sec, _is_pm; 
	
 	public: 

// Metodos
		/*Inicializa o Clock*/	 
		void setClock (int h, int s, int m, bool pm); 

		/*Le o valor atual de clock*/
		void readClock (int& h, int& s, int& m, bool& pm); 

		/*Avanca em um mili segundo o clock*/
	 	void advance (); 

}; 
