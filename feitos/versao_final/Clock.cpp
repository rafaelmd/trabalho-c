/*Inicializa o Clock*/
void Clock::setClock (int h, int s, int m, bool pm)
{

	_hr = h;
	_sec = s;
	_min = m;
	_is_pm = pm;
	
}

/*Le o valor atual de clock*/
void Clock::readClock (int& h, int& s, int& m, bool& pm)
{
	h = _hr;
	m = _min;
	s = _sec;
	pm = _is_pm; 
}

/*Avanca em um mili segundo o clock*/
void Clock::advance ()
{
	
	if (_sec<599) _sec++;
		
	else 
		
		if (_min<59)
		{
			_min++;
			_sec=0;
		}
		
		else
			
			if (_hr<11)
			{
				_hr++;
				_min=0;
			}
		
			else
				
				if (_is_pm==1) 
				{
					_is_pm=0;
					_hr=0;
					_min=0;
					_sec=0;
				}	
				else 
				{
					_is_pm=1;
					_hr=0;
					_min=0;
					_sec=0;
				}	
};
