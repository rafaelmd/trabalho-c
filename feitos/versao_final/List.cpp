/*
   File List.cpp

   Class List stores Nodes (class Node) in a linked list.
   
   This file has the implementation for class List

   Class list is definition for a linked list, having the following operations:

   1. Initialize the list to an empty list.
   2. Free the nodes of a list.
   3. Determine whether a list is empty.
   4. Add a node with a given value into the list following
      the first node with another given value.
   5. Add a node with a given value to the front of the list.
   6. Add a node with a given value to the end of the list.
   7. Delete the first node from the list.

   Eduardo Augusto Bezerra <bezerra@inf.pucrs,br>
   Faculdade de Informatica, PUC-RS
   Laboratorio de Programacao II (EC)

   Abril de 2006.

*/

#include "List.h"

List::List() 
{
	head = 0;
	tail = 0;
  	atual = 0;
}


List::~List() 
{
	Node* cursor = head;
	while(head) 
	{
		cursor = cursor->getNext();
		delete head;
		head = cursor;
  	}
  		head = 0; // Officially empty
}


void List::insertBeforeFirst(int dat) 
{
	Node* aux = head; // salva o valor do head atual
	head = new Node(dat, head, 0); // head recebe a posição do novo nó
	atual = head;

	if (aux) aux->setLast(head);

	if(tail==0) tail=head;
}

int List::readAtual() 
{
	return atual->getVal(); 
}


void List::setAtual(int direct) {
   
	if(atual==0)
	{
	        cout << "Lista Vazia" << endl; 
	        return;
	}
	if (direct==1)
	{
	        if(atual==tail)
		{
	        	atual=head;
       	        }
        	else 
		{    
		        atual = atual->getNext();
	        }     
        } 
	else
        
		if(atual==head)
		{           
			atual=tail;
        	}
        	else
		{
			atual= atual->getLast();
		}    
}


void List::setTime(int h, int s, int m, bool pm, int mn, int d, int y) 
{
	atual->setClock(h, s, m, pm);
	atual->setCalendar( mn, d, y);
}


void List::readTime(int& h, int& s, int& m, bool& pm, int& mn, int& d, int& y) 
{
	atual->readClock(h, s, m, pm);
	atual->readCalendar(mn, d, y);
}


