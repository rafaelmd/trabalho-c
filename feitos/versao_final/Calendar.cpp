/*Inicializa o Calendario*/
void Calendar::setCalendar (int m, int d, int y)
{	
	_mo = m;
	_day = d;
	_yr = y;
}

void Calendar::readCalendar (int& m, int& d, int& y)
{
	m = _mo;
	d = _day;
	y = _yr; 
}

void Calendar::advance () 
{	
	if (_day<30)
	{
		_day++;
	}
	else 
	{		
		if (_mo<12)
		{
			_mo++;
			_day=1;
		}
		else
		{
			_yr++;
			_mo=1;
			_day=1;
		}
	}
}
