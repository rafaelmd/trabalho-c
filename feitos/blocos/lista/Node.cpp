/*
   File Node.cpp

   Class Node

   Class Node stores integer values for a linked list.
   
   This file has the implementation for the Node's interface.

   Eduardo Augusto Bezerra <bezerra@inf.pucrs,br>
   Faculdade de Informatica, PUC-RS
   Laboratorio de Programacao II (EC)

   Abril de 2006.

*/

#include "Node.h"

// Constructor - initializes the node
//
Node::Node(int dat, Node* nxt, Node* lst){
       val = dat;
       next = nxt;
       last = lst;
}

// getVal returns the integer value stored in the node
//
int Node::getVal(){
     return val;
}

// getNext returns a pointer for the next node in the linked list
//
Node* Node::getNext(){
     return next;
}

// setVal stores the integer value in the node
//
void Node::setVal(int dat){
     val = dat;
}

// setNext stores the pointer to the next node in the list in the "next" field
//
void Node::setNext(Node* nxt){
       next = nxt;
}

void Node::setLast(Node* lst){
       last = lst;
}

Node* Node::getLast(){
     return last;
}