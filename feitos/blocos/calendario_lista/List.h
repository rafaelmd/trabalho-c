/*
   File List.h

   Header file for class list

   Class List stores Nodes (class Node) in a linked list.
   
   This file has the List's interface (header file).

   Class list is definition for a linked list, having the following operations:

   1. Initialize the list to an empty list.
   2. Free the nodes of a list.
   3. Determine whether a list is empty.
   4. (NOT YET!) Add a node with a given value into the list following
      the first node with another given value.
   5. Add a node with a given value to the front of the list.
   6. Add a node with a given value to the end of the list.
   7. Delete the first node from the list.

   Eduardo Augusto Bezerra <bezerra@inf.pucrs,br>
   Faculdade de Informatica, PUC-RS
   Laboratorio de Programacao II (EC)

   Abril de 2006.

*/

#include "Node.cpp"

class List : public ClockCalendar {

  Node* head;
  Node* tail;

  Node* atual;

public:
  List();
  ~List();
  void insertBeforeFirst(int dat);

//  void insertAfterLast(int dat);

  int readAtual(); // lê o nó atual da lista


  //int getAtual(); // lê o primeiro nó da lista

  void setAtual(int direct); // lê o primeiro nó da lista

//  int removeFirst();

//  void insertionSort(int value);
//  int removeNode(int dat);

  void listAll();

  void readTime (int& h, int& s, int& m, bool& pm);
  void setTime(int h, int s, int m, bool pm);
};

// métodos comentados foram retirados do exemplo do bezerra pois não são necessários