/*
   File Node.h

   Class Node

   Class Node stores integer values for a linked list.
   
   This file has the Node's interface (header file).

   Eduardo Augusto Bezerra <bezerra@inf.pucrs,br>
   Faculdade de Informatica, PUC-RS
   Laboratorio de Programacao II (EC)

   Abril de 2006.

*/

#include "headers.h"

class Node : public ClockCalendar {

    int val;
    Node* next;
    Node* last;

  public:
         
    Node(int dat, Node* nxt, Node* lst);
    int getVal();

    Node* getNext();
    Node* getLast();

    void setVal(int dat);
    void setNext(Node* nxt);
    void setLast(Node* lst);
};
