#include <iostream>
#include "calendario.h"
#include <time.h>
#include <unistd.h>
#include "clock.cpp"
#include "calendar.cpp"
#include "clockcalendar.cpp"

#ifdef LINUX_KEY_WORD
#include "clockcalendarlinux.h"
#include "clockcalendarlinux.cpp"
ClockCalendarLinux calendario;
#else
#include "clockcalendaratlys.h"
#include "clockcalendaratlys.cpp"
ClockCalendarAtlys calendario;
#endif
using namespace std;



/* void sleep(int mseconds) 
{ 
	clock_t goal = mseconds + clock(); 
	while (goal > clock()); 
}
*/
int main(int c, char **v){

	


	calendario.setClock(9,0,9,0);
	calendario.setCalendar(10,15,2014);

	while (true) {
		int h, m, s, d, mn, y;
		bool pm;
		string pm_show;

		calendario.readClock(h, s, m, pm);
		
		if(pm==1){pm_show=" PM";}
		else{pm_show=" AM";}

			
		cout << h << ':' << m << ':' << s <<  pm_show << endl;

		calendario.readCalendar(mn, d, y);
			
		cout << d << '/' << mn << '/' << y << endl;

		calendario.advance();
		calendario.sleep(1000000);
	}
}
