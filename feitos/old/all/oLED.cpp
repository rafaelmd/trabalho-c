#include <iostream>
#include "comandos.h"
#include "caracteres.h"
#include <unistd.h>

#define CS 24
#define MOSI 25
#define SCK 27
#define DC 28
#define RES 29
#define VBATC 30
#define VDDC 31

#include <ctime>

#define	cmdOledDisplayOn	0xAF
#define	cmdOledDisplayOff	0xAE
#define	cmdOledSegRemap		0xA1	//map column 127 to SEG0
#define	cmdOledComDir		0xC8	//scan from COM[N-1] to COM0
#define	cmdOledComConfig	0xDA	//set COM hardware configuration


inline void delayms(clock_t aux) // clock_t is a like typedef unsigned int clock_t. Use clock_t instead of integer in this context
{

clock_t msec=860*aux;
clock_t start_time = clock();
clock_t end_time = msec  + start_time;
while(clock() != end_time);
} 

inline void delay_cem_us(clock_t aux) // clock_t is a like typedef unsigned int clock_t. Use clock_t instead of integer in this context
{

clock_t msec=86*aux;
clock_t start_time = clock();
clock_t end_time = msec  + start_time;
while(clock() != end_time);
} 

void lcd_send_command(int comando)
{	
		unsigned int bit=0x80;
		*output&=~(1<<DC);						// DC=0 Entra no modo de comando
		*output&=~(1<<CS);  					// CS=0 Chip select input (active low)
		delay_cem_us(1);

		for(int j=0;j<=7;j++)
		{				
					*output&=~(1<<SCK);			//SCK=0
					
					if(comando & (bit>>j))		//MISO em nivel alto ou baixo depende do dado a enviar
						*output|=(1<<MOSI);     //envia 1
					else
						*output&=~(1<<MOSI);	//envia 0
						
					delay_cem_us(1);					//delay para garantir integridade do bit
					
					*output|=(1<<SCK);  		//SCK=1
					delay_cem_us(1);
					
		}	
		*output&=~(1<<SCK);						//SCK=0
		*output&=~(1<<MOSI);					//MOSI=0
		*output|=(1<<CS);  						//CS=1
}



void lcd_send_data(char dado)
{
		unsigned int bit=0x80;
		*output|=(1<<DC);						//DC=1 Entra no modo de dados
		*output&=~(1<<CS);  					// nivel baixo Slave Select
		delay_cem_us(1);							//delay ajustar de acordo com clock
		
		for(int j=0;j<=7;j++)			//rotaciona bit=1000 0000 , 
		{
					*output&=~(1<<SCK);			//Serial clock em nivel baixo
					
					if(dado & (bit>>j) )					//MISO em nivel alto ou baixo depende do dado a enviar
						*output|=(1<<MOSI);
					else
						*output&=~(1<<MOSI);
					
					delay_cem_us(1);
					
					*output|=(1<<SCK);  		//nivel alto serial clock
					delay_cem_us(1);
					
		}	
		*output&=~(1<<SCK);	
		*output&=~(1<<MOSI);
		*output|=(1<<CS);  						 //nivel alto Slave Select
}

void poweron2(void)
{
	*output|=(1<<RES);					// seta sa�da de reset = 1	
	*output|=(1<<VDDC);					// seta vdd control = 0	
	*output|=(1<<VBATC);				//seta b vattery control = 1	
	delayms(1);
	
	*output&=(1<<DC);
	*output&=~(1<<VDDC);				// Vdd control=0 (permite alimenta��o do display)
	delayms(100);
	
	lcd_send_command(SSD1306_DISPLAYOFF);
	
	// reseta o display
	*output&=~(1<<RES);					// seta sa�da de reset = 0
	delay_cem_us(1);
	*output|=(1<<RES);
	
		lcd_send_command(0x8D);        // send display off command
		lcd_send_command(0x14);
		
		lcd_send_command(0xD9);
		lcd_send_command(0xF1);
		
	delay_cem_us(1);
	*output&=~(1<<VBATC);					//VBAT=0	
	delayms(100);
	
	lcd_send_command(SSD1306_SEGREMAP);
	lcd_send_command(SSD1306_COMSCANINC);
	
		lcd_send_command(cmdOledComConfig);
			lcd_send_command(0X20);
	
	lcd_send_command(SSD1306_DISPLAYON);
	delayms(100);  	      
}

void demo_fill_screen()
{
	unsigned char i,j;
	//=====Set internal RAM to FFH=========//
	for(i=4;i<8;i++)	
	{
		lcd_send_command (0xb0+ i );	 // Set Page
		lcd_send_command (0x00);
		lcd_send_command (0x10);
		
		for(j=0;j<128;j++)				// data of the page
			{
				lcd_send_data(0xFF);
				delay_cem_us(1);
			}
		
	}		
}
void clear_screen()
{
	unsigned char i,j;
	//=====Set internal RAM to FFH=========//
	for(i=4;i<8;i++)	
	{
		lcd_send_command (0xb0+ i );	 // Set Page
		lcd_send_command (0x00);
		lcd_send_command (0x10);
		
		for(j=0;j<128;j++)				// data of the page
			{
				lcd_send_data(0x00);
				delayms(10);
			}
		//	std::cout<<"linha:"<<i<<"\n"<<std::endl;
	}		
}

void sendsimbol(char *vetor)
{
	for(int j=0;j<8;j++)
	{
		lcd_send_data(vetor[j]);
		
	}
}
void sendchar(char simbol)
{	int *aux;
	aux=&caracteres[0] +8*simbol;
	for(int j=0;j<8;j++)
	{	
		
		lcd_send_data(*(aux+j));
		
	}
}
void sendString(char string[])
{	char k=0;
	do
	{
		
		sendchar(string[k]);
		k++;
		
	}while(string[k]!=0);
	
}

void display(int valor, int h, int m, int s, bool pm, int mn, int d, int y)
{
	char buffer [50];
	
	sprintf (buffer, "Reg: %d        ", valor);

		lcd_send_command (0xb4);	 // Set Page
		lcd_send_command (0x00);
		lcd_send_command (0x10);
		sendString(buffer);
	
	if(pm==1){
	sprintf (buffer, "Hr-%d: %d: %d  PM ", h, m, s/10);
	}
	else{
	sprintf (buffer, "Hr-%d: %d: %d  AM ", h, m, s/10);
	}
	
		lcd_send_command (0xb5);	 // Set Page
		lcd_send_command (0x00);
		lcd_send_command (0x10);
		sendString(buffer);

		
	sprintf (buffer, "D-%d/ %d/ %d", mn, d, y);
			lcd_send_command (0xb6);	 // Set Page
		lcd_send_command (0x00);
		lcd_send_command (0x10);
		sendString(buffer);
		
	sprintf (buffer, "----------------");
			lcd_send_command (0xb7);	 // Set Page
		lcd_send_command (0x00);
		lcd_send_command (0x10);
		sendString(buffer);

}